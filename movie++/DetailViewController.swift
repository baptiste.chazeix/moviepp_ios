//
//  DetailViewController.swift
//  movie++
//
//  Created by Baptiste CHAZEIX on 02/06/2020.
//  Copyright © 2020 Baptiste CHAZEIX. All rights reserved.
//

import UIKit
import MovieFramework

class DetailViewController: UIViewController {
    
    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var voteLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var overviewTextView: UITextView!
    
    var dataTask: URLSessionDataTask?
    
    func configureView() {
        // Update the user interface for the detail item.
        if let movie = movie {
            if let label = titleLable {
                label.text = String(movie.id)
                startLoadingMovie()
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureView()
    }
    
    var movie: Movie? {
        didSet {
            // Update the view.
            configureView()
        }
    }
    
    fileprivate func parseData(_ jsonData: Data) {
        let decoder = JSONDecoder()
        do {
            let movie = try decoder.decode(MovieDetail.self, from: jsonData)
            DispatchQueue.main.async { [weak self] in
                self?.titleLable.text = movie.title
                self?.dateLabel.text = movie.release_date.replacingOccurrences(of: "-", with: "/")
                self?.voteLabel.text = String(movie.vote_average) + "/10"
                self?.overviewTextView.text = movie.overview
            }
        } catch {
            fatalError("TODO show error to user")
        }
    }
    
    func startLoadingMovie() {
        guard let movie = movie else { return }
        if let url = URL(string: movie.APILink) {
                dataTask = URLSession.shared.dataTask(with: url) { [weak self] (data, response, error) in
                    guard let strongSelf = self else { return }
                    if let error = error {
                        fatalError("TODO show error to user: \(error)")
                    } else if let jsonData = data {
                        strongSelf.parseData(jsonData) //change to result
                    }
                }
                dataTask?.resume()
            } else {
                fatalError("TODO show error to user")
            }
        
    }


}

