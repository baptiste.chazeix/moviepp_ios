//
//  movie.swift
//  movie++
//
//  Created by Baptiste CHAZEIX on 09/06/2020.
//  Copyright © 2020 Baptiste CHAZEIX. All rights reserved.
//

import Foundation

public struct Movie {
    public let id: Double
    public let title: String
    
    init?(line: String) {
        guard line.isEmpty == false else {
            return nil
        }
        let columns = line.components(separatedBy: "/")
        guard columns.count >= 2 else {
            return nil
        }
        guard let id = Double(columns[0]) else {
            return nil
        }
        self.id = id
        self.title = columns[1]
    }

    public var APILink: String {
        return "https://api.themoviedb.org/3/movie/\(id)?api_key=2ececd07e83a3e996743a05700344f7e&language=en-US"
    }
}
