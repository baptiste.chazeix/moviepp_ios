//
//  MovieLoader.swift
//  MovieFramework
//
//  Created by Baptiste CHAZEIX on 20/06/2020.
//  Copyright © 2020 Baptiste CHAZEIX. All rights reserved.
//

import Foundation

enum MovieLoaderError: Error {
    case emptyCSV
    case invalidLine(line: Int)
}

public class MovieLoader {
    
    public var movies = [Movie]()
    
    public init() {
    }
    
    @discardableResult
    public func loadMovies(path: String) throws -> Int {
        let csvContent = try String(contentsOfFile: path)
        
        let lines = csvContent.components(separatedBy: "\n")

        guard lines.count > 0 else {
            throw MovieLoaderError.emptyCSV
        }
        
        for (index, line) in lines.enumerated() where index > 0 && line.isEmpty == false {
            if let movie = Movie(line: line) {
                movies.append(movie)
            } else {
                throw MovieLoaderError.invalidLine(line: index + 1)
            }
        }
        
        return movies.count
    }
}
