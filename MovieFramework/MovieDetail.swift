//
//  MovieDetail.swift
//  movie++
//
//  Created by Baptiste CHAZEIX on 09/06/2020.
//  Copyright © 2020 Baptiste CHAZEIX. All rights reserved.
//

import Foundation

public struct MovieDetail: Codable{
    public var overview: String
    public var release_date: String
    public var title: String
    public var vote_average: Double
}
