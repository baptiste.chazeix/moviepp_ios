//
//  MovieFramework.h
//  MovieFramework
//
//  Created by Baptiste CHAZEIX on 20/06/2020.
//  Copyright © 2020 Baptiste CHAZEIX. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for MovieFramework.
FOUNDATION_EXPORT double MovieFrameworkVersionNumber;

//! Project version string for MovieFramework.
FOUNDATION_EXPORT const unsigned char MovieFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MovieFramework/PublicHeader.h>


